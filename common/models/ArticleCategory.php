<?php

namespace common\models;

use common\models\query\ArticleCategoryQuery;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\Article;

/**
 * This is the model class for table "article_category".
 *
 * @property integer $id
 * @property string $slug
 * @property string $title
 * @property integer $status
 *
 * @property Article[] $articles
 * @property ArticleCategory $parent
 */
class ArticleCategory extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DRAFT = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_category}}';
    }

    /**
     * @return ArticleCategoryQuery
     */
    public static function find()
    {
        return new ArticleCategoryQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'immutable' => true
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 512],
            [['slug'], 'unique'],
            [['slug'], 'string', 'max' => 1024],
            ['status', 'integer'],
            ['parent_id', 'exist', 'targetClass' => ArticleCategory::className(), 'targetAttribute' => 'id']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'slug' => Yii::t('common', 'Slug'),
            'title' => Yii::t('common', 'Title'),
            'parent_id' => Yii::t('common', 'Parent Category'),
            'status' => Yii::t('common', 'Active')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasMany(ArticleCategory::className(), ['id' => 'parent_id']);
    }

    /**
     * @param integer $parent_id
     * @return \yii\db\ActiveQuery
     */
    public static function getChildren($parent_id)
    {
        return self::find()
            ->where('parent_id = ' . $parent_id . ' AND status = 1')
            ->all();
    }


    /**
     * @return array
     */
    public static function getItemsForMenu() {

        $result = array();
        $listCategory = self::find()->where('status = 1 AND parent_id IS NULL')->all();
        if (count($listCategory) > 0) {
            foreach ($listCategory as $key => $category) {
                $result[$key] = [
                    'label' => $category->title,
                    'url' => [
                        'article-category/index',
                        'slug' => $category->slug
                    ],
                ];
                $children = self::getChildren($category->id);
                if (count($children) > 0) {
                    foreach ($children as $child) {
                        $articles = Article::find()->where(['category_id' => $child->id])->limit(4)->orderBy('updated_at DESC')->asArray()->all();
                        $str = '';
                        foreach ($articles as $article) {
                            $str .= '
                            <li class="menu-post-container">
                                <a href="/article/'.$article['slug'].'"
                                   rel="bookmark">
                                    <div class="mmp-img">
                                        <img
                                            src="'.$article['thumbnail_base_url'].'/'.$article['thumbnail_path'].'"
                                            srcset="'.$article['thumbnail_base_url'].'/'.$article['thumbnail_path'].' 760w, '.$article['thumbnail_base_url'].'/'.$article['thumbnail_path'].' 190w"
                                            sizes="(max-width: 380px) 100vw, 380px" alt="" width="760" height="500">
                                    </div>
                                    <h6>'.$article['title'].'</h6>
                                </a>
                            </li>
                            ';
                        }
                        $result[$key]['items'][] = [
                            'label' => $child->title,
                            'template' => '
                            <li id="child_'.$child->id.'" class="menu-item menu-item-type-taxonomy menu-item-object-category">
                                <a href="/'.$child->slug.'">{label}</a>
                            </li>
                            <li class="tab-content clearfix child_'.$child->id.'">
                                <ul class="mnky-menu-posts">'.$str.'
                                </ul>
                            </li>
                            ',
                        ];
                    }
                }
            }

        }
        return $result;
    }
}
