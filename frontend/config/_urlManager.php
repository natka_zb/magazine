<?php
return [
    'class'=>'yii\web\UrlManager',
    'enablePrettyUrl'=>true,
    'showScriptName'=>false,
    'rules'=> [
        // Pages
        ['pattern'=>'page/<slug>', 'route'=>'page/view'],

        // Categories
        ['pattern'=>'<slug>', 'route'=>'article-category/index'],


        // Articles
        ['pattern'=>'<category>/<slug>', 'route'=>'article/view'],
        ['pattern'=>'article/attachment-download', 'route'=>'article/attachment-download'],


        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']]
    ]
];