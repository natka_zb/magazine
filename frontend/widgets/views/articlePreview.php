<?php
$N = 1;
?>
<article id="post-813" class="clearfix post-813 page type-page status-publish" style="transform: none;">
    <div class="entry-content clearfix" style="transform: none;">
        <div class="vc_row wpb_row vc_row-fluid" style="transform: none;">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="mnky-posts clearfix mp-layout-5 mp-two-column">
                            <?php
                            foreach ($articles as $oneArticle) {
                                ?>
                                <div class="mp-container mp-post-<?=$N?> mp-post-main" itemscope="" itemtype="http://schema.org/Article">
                                    <div class="mp-category">
                                        <a href="/<?=$oneArticle->category->slug?>" rel="category tag"><?=$oneArticle->category->title?></a>
                                        ,
                                        <a href="/<?=$oneArticle->category->slug?>" rel="category tag"><?=$oneArticle->category->title?></a>
                                    </div>
                                    <h2 class="mp-title" itemprop="headline">
                                        <a itemprop="mainEntityOfPage" href="/article/<?=$oneArticle->slug?>" title="<?=$oneArticle->title?>" rel="bookmark">
                                            <?=$oneArticle->title?>
                                        </a>
                                    </h2>
                                    <div class="mp-article-meta">
                                        <span class="mp-author">
                                            <a class="author-url" href="http://mnkythemes.com/hush/author/katyharvill/" title="View all posts by <?=$oneArticle->author->username?>" rel="author">
                                                <span itemprop="author" itemscope="" itemtype="http://schema.org/Person">
                                                    <span itemprop="name">By <?=$oneArticle->author->username?></span>
                                                </span>
                                            </a>
                                        </span>
                                        <span class="mp-date">
                                            <time datetime="<?=Yii::$app->formatter->asDate($oneArticle->published_at,"Y-m-dd H:i")?>" itemprop="datePublished">, <?=Yii::$app->formatter->asDate($oneArticle->published_at,"dd.MM.Y")?></time>
                                            <time class="meta-date-modified" datetime="<?=Yii::$app->formatter->asDate($oneArticle->updated_at,"Y-m-dd H:i")?>" itemprop="dateModified"><?=Yii::$app->formatter->asDate($oneArticle->updated_at,"dd.MM.Y")?></time>
                                        </span>
                                        <span class="mp-views">
                                            <span class="views-hot" title="Views">
                                                <i class="fa fa-bolt"></i>
                                                917
                                                <meta itemprop="interactionCount" content="UserPageVisits:917">
                                            </span>
                                        </span>
                                    </div>
                                    <a class="mp-image" href="/article/<?=$oneArticle->slug?>" title="<?=$oneArticle->title?>" rel="bookmark">
                                        <div itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
                                            <img src="<?=$oneArticle->thumbnail_base_url?>/<?=$oneArticle->thumbnail_path?>" srcset="<?=$oneArticle->thumbnail_base_url?>/<?=$oneArticle->thumbnail_path?> 1200w, <?=$oneArticle->thumbnail_base_url?>/<?=$oneArticle->thumbnail_path?> 300w" sizes="(max-width: 600px) 100vw, 600px" alt="" width="600" height="400">
                                            <meta itemprop="url" content="<?=$oneArticle->thumbnail_base_url?>/<?=$oneArticle->thumbnail_path?>">
                                            <meta itemprop="width" content="1600">
                                            <meta itemprop="height" content="1100">
                                        </div>
                                    </a>
                                </div>
                                <?php
                                $N++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
