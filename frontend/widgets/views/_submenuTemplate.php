<?php
?>
<ul class="sub-menu">
    <li>{items}</li>
    <?php
    foreach ($articles as $article) {
        ?>
        <li class="tab-content menu-item-225 clearfix <?=$article['id']?>">
            <ul class="mnky-menu-posts mmp-4">
                <?php
                if (isset($article['itemsContent'])) {
                    foreach ($article['itemsContent'] as $oneContent) {
                        ?>
                        <li class="menu-post-container">
                            <a href="/article/<?=$oneContent['slug']?>"
                               rel="bookmark">
                                <div class="mmp-img">
                                    <img
                                        src="<?=$oneContent['thumbnail_base_url']?>/<?=$oneContent['thumbnail_path']?>"
                                        srcset="<?=$oneContent['thumbnail_base_url']?>/<?=$oneContent['thumbnail_path']?> 760w, <?=$oneContent['thumbnail_base_url']?>/<?=$oneContent['thumbnail_path']?> 190w"
                                        sizes="(max-width: 380px) 100vw, 380px" alt="" width="380" height="250">
                                </div>
                                <h6><?=$oneContent['title']?></h6>
                            </a>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </li>
        <?php
    }
    ?>
</ul>
