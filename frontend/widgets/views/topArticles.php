<?php
use yii\helpers\Html;

/**
 * @param common\models\Article $articles
 */
?>
<?php if (!empty($articles) && is_array($articles)): ?>
<div class="most-views">
    <ol>
        <?php foreach ($articles as $key => $article): ?>
        <li class="most_pop_<?=($key % 2) ? 'even':'odd'?>">
            <?=Html::a(
                Html::img(
                    Yii::$app->glide->createSignedUrl(
                        [
                            'glide/index',
                            'path' => $article->thumbnail_path,
                            'w' => 100,
                            'h' => 100,
                            'fit' => 'crop'
                        ],
                        true
                    ),
                    ['class' => 'attachment-slideshow-more-img wp-post-image']
                ),
                ['article/view', 'slug' => $article->slug, 'category' => $article->category->slug]
            )?>
            <?=Html::a($article->title, ['article/view', 'slug' => $article->slug, 'category' => $article->category->slug], [
                'class' => 'link_most_views'
            ])?>
        </li>
        <?php endforeach; ?>


    </ol>
</div>
<div class="global_primary_unit" style="display: none;">
    <li class="most-popular-native-content-0 most_pop_even">
        <a class="js-track-url"
           href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/"
           data-tablet-href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/"
           data-mobile-href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/">

            <div class="most-image-warpper"><img
                    src="http://www.self.com/wp-content/uploads/2016/08/IMG_9109-220x220.jpg" alt="IMG_9109"
                    data-lazy-loaded="false"/></div>
        </a>

        <div class="link_most_views">
            <div class="slf-sponsored-name">
                <span class="sp-title">Sponsored Content:</span>
                <span class="sp-name">H&M Sport</span>
            </div>
            <a class="link_most_views js-track-url"
               href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/"
               data-tablet-href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/"
               data-mobile-href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/">Where
                a Woman Who Teaches 15 Cycling Classes a Week Finds Her Motivation—And Where You Can Find Yours</a>
        </div>
    </li>

    <div style="display: none;">
        <input type="text" class="datapos60x60" value="0"/>

        <div id="global_primary_unit60x60_frame"></div>
        <script type="text/javascript">
            CN.dart.call("global_primary_unit", {sz: "60x60", kws: [""]});
        </script>
    </div>
    <li class="most-popular-native-content-1 most_pop_even">
        <a class="js-track-url" href="http://www.self.com/native-article/my-best-accessory/"
           data-tablet-href="http://www.self.com/native-article/my-best-accessory/"
           data-mobile-href="http://www.self.com/native-article/my-best-accessory/">

            <div class="most-image-warpper"><img
                    src="http://www.self.com/wp-content/uploads/2016/06/20160607-25-220x220-1466606482.jpg"
                    alt="20160607-25" data-lazy-loaded="false"/></div>
        </a>

        <div class="link_most_views">
            <div class="slf-sponsored-name">
                <span class="sp-title">Sponsored Content</span>
                <span class="sp-name">G-Shock</span>
            </div>
            <a class="link_most_views js-track-url" href="http://www.self.com/native-article/my-best-accessory/"
               data-tablet-href="http://www.self.com/native-article/my-best-accessory/"
               data-mobile-href="http://www.self.com/native-article/my-best-accessory/">My Best Accessory </a>
        </div>
    </li>

    <div style="display: none;">
        <input type="text" class="datapos90x92" value="1"/>

        <div id="global_primary_unit90x92_frame"></div>
        <script type="text/javascript">
            CN.dart.call("global_primary_unit", {sz: "90x92", kws: [""]});
        </script>
    </div>
</div>
<?php endif;?>