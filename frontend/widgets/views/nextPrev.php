<?php
use yii\helpers\Html;
?>
<?php if (!empty($prev) || !empty($next)): ?>
    <div class="pagination-wrap span8">
        <?php if (!empty($prev)): ?>
            <div class="oldpost-box fa fa-angle-left">
                <div class="pagination-thumb-left">
                    <?php echo Html::img(
                        Yii::$app->glide->createSignedUrl(
                            [
                                'glide/index',
                                'path' => $prev->thumbnail_path,
                                'w' => 65,
                                'h' => 65,
                                'fit' => 'crop'
                            ],
                            true
                        ),
                        ['class' => 'attachment-slideshow-more-img wp-post-image']
                    )?>
                </div>
                <div class="oldpost-box">
                    <a rel="next" href="<?=\yii\helpers\Url::to(['article/view', 'slug' => $prev->slug, 'category'=> $prev->category->slug])?>">
                        <div class="blog_navi">
                            <div class="blog_navi_title">
                                <?=strip_tags($prev->title)?>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($next)): ?>
            <div class="newpost-box fa fa-angle-right">

                <div class="pagination-thumb-right">
                    <?php echo Html::img(
                        Yii::$app->glide->createSignedUrl(
                            [
                                'glide/index',
                                'path' => $next->thumbnail_path,
                                'w' => 65,
                                'h' => 65,
                                'fit' => 'crop'
                            ],
                            true
                        ),
                        ['class' => 'attachment-slideshow-more-img wp-post-image']
                    )?>
                </div>
                <div class="newpost-box">
                    <a rel="next" href="<?=\yii\helpers\Url::to(['article/view', 'slug' => $next->slug, 'category'=> $next->category->slug])?>">
                        <div class="blog_navi">
                            <div class="blog_navi_title">
                                <?=strip_tags($next->title)?>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>