<?php
?>
<h3 class="widget-title">Categories</h3>
<ul>
    <?php
    foreach ($categories as $oneCategory) {
        ?>
        <li class="cat-item cat-item-5">
            <a href="/<?=$oneCategory['url']['slug']?>" ><?=$oneCategory['label']?></a>
        </li>
        <?php
    }
    ?>
</ul>
