<?php
namespace app\widgets;
use common\models\Article;
use yii\base\Widget;


/**
 * Class TopArticlesWidget
 * @package common\models\Article $article
 * @package app\widgets
 */
class TopArticlesWidget extends Widget
{
    public $article;
    public $template;
    public $count;

    public function init()
    {
        parent::init();
        if (empty($this->template)) {
            $this->template = 'topArticles';
        }
    }

    public function run()
    {
        $articles = Article::find()->with('category')->where('category_id=' . $this->article->category_id . ' AND status=1 AND id != ' . $this->article->id)->orderBy('id DESC')->limit($this->count)->all();
        return $this->render($this->template, [
            'articles' => $articles
        ]);
    }
}