<?php
namespace app\widgets;

use yii\base\Widget;
use common\models\ArticleCategory;

/**
 * Class MenuBottomWidget
 * @package common\models\Article $article
 * @package app\widgets
 */
class MenuBottomWidget extends Widget
{
    public $template;
    public $count;

    public function init()
    {
        parent::init();
        if (empty($this->template)) {
            $this->template = 'menuBottom';
        }
    }

    public function run()
    {
        $categories = ArticleCategory::getItemsForMenu();
        return $this->render($this->template, [
            'categories' => $categories
        ]);
    }
}
