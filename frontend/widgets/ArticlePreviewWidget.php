<?php
namespace app\widgets;

use yii\base\Widget;
use common\models\Article;

/**
 * Class ArticlePreviewWidget
 * @package common\models\Article $article
 * @package app\widgets
 */
class ArticlePreviewWidget extends Widget
{
    public $template;
    public $count;

    public function init()
    {
        parent::init();
        if (empty($this->template)) {
            $this->template = 'articlePreview';
        }
    }

    public function run()
    {
        $articles = Article::find()
            ->with('category')
            ->where('status=1')->orderBy('updated_at DESC')->limit($this->count)->all();
        return $this->render($this->template, [
            'articles' => $articles
        ]);
    }
}
