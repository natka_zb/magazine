<?php
namespace app\widgets;

use yii\base\Widget;
use common\models\ArticleCategory;
use Yii;

/**
 * Class MenuTopWidget
 * @package common\models\Article $article
 * @package app\widgets
 */
class MenuTopWidget extends Widget
{
    public $template;
    public $count;

    public function init()
    {
        parent::init();
        if (empty($this->template)) {
            $this->template = 'menuTop';
        }
    }

    public function run()
    {
        $itemsMenu = ArticleCategory::getItemsForMenu();
        $menu = [
            'items' => array_merge(
                $itemsMenu,
                [
                    [
                        'label' => Yii::t('frontend', 'Settings'),
                        'url' => ['/user/default/index']
                    ],
                    [
                        'label' => Yii::t('frontend', 'Backend'),
                        'url' => Yii::getAlias('@backendUrl'),
                        'visible' => Yii::$app->user->can('manager')
                    ],
                    [
                        'label' => Yii::t('frontend', 'Logout'),
                        'url' => ['/user/sign-in/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ]
                ]
            ),
            'options' => [
                'id' => 'menu-main-menu',
                'class' => 'menu',
            ],
            'itemOptions' => [
                'class' => 'megamenu-tabs menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children'
            ],
            'submenuTemplate' => '<ul class="sub-menu">{items}</ul>'
        ];
        return $this->render($this->template, [
            'menu' => $menu
        ]);
    }
}
