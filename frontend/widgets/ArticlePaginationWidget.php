<?php
namespace app\widgets;
use common\models\Article;
use yii\base\Widget;


/**
 * Class ArticlePaginationWidget
 * @package common\models\Article $article
 * @package app\widgets
 */
class ArticlePaginationWidget extends Widget
{
    public $article;
    public $template;

    public function init()
    {
        parent::init();
        if (empty($this->template)) {
            $this->template = 'nextPrev';
        }
    }

    public function run()
    {

        /*$criteria = new CDbCriteria();
        $criteria->with = 'site_article';
        $criteria->params = array(':site_id'=>$this->controller->siteParams['siteId']);
        $criteria->together = true;
        $criteria->order = 'site_article.article_id DESC';
        $criteria->condition = "site_article.`site_id` = :site_id and t.`status`='published' and t.id < $this->article_id ";

        $articlePrev = Articles::model()->find($criteria);

        if (empty($articlePrev))
        {
            $criteria->condition = 'site_article.`site_id` = :site_id AND `t`.`status`=\'published\'';
            $articlePrev = Articles::model()->find($criteria);
        }

        $criteria->condition = "site_article.`site_id` = :site_id and t.`status`='published' and t.id > $this->article_id ";
        $criteria->order = 'site_article.article_id ASC';

        $articleNext = Articles::model()->find($criteria);

        if (empty($articleNext))
        {
            $criteria->condition = 'site_article.`site_id` = :site_id AND `t`.`status`=\'published\'';
            $articleNext = Articles::model()->find($criteria);
        }*/



        $prev = Article::find()->with('category')->where('category_id=' . $this->article->category_id . ' AND id < ' . $this->article->id . ' AND status=1')->orderBy('id DESC')->one();
        $next = Article::find()->with('category')->where('category_id=' . $this->article->category_id . ' AND id > ' . $this->article->id . ' AND status=1')->orderBy('id ASC')->one();

        return $this->render($this->template, [
            'prev' => $prev,
            'next' => $next
        ]);
    }
}