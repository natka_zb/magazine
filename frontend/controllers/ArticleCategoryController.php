<?php

namespace frontend\controllers;

use common\models\Article;
use Yii;
use common\models\ArticleCategory;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ArticleCategoryController implements the CRUD actions for ArticleCategory model.
 */
class ArticleCategoryController extends Controller
{

    /**
     * Lists all ArticleCategory models.
     * @param string $slug
     * @return mixed
     */
    public function actionIndex($slug)
    {
        $model = $this->findModel($slug);

        $template = 'index';

        //if this parent category -> view indexCat
        /*if ($model->parent_id === null) {
            $dataProvider = new ActiveDataProvider([
                'query' => ArticleCategory::find()->where('parent_id=' . $model->id),
                'pagination' => [
                    'defaultPageSize' => 6,
                ],
            ]);
            $template = 'indexCat';
        } else {*/
            $dataProvider = new ActiveDataProvider([
                'query' => Article::find()->with(['category', 'author'])->where('category_id=' . $model->id)->orderBy('updated_at DESC'),
                'pagination' => [
                    'defaultPageSize' => 6,
                ],
            ]);
        //}

        return $this->render(
            $template,
            [
                'model' => $model,
                'dataProvider' => $dataProvider
            ]
        );
    }

    /**
     * Displays a single ArticleCategory model.
     * @param string $slug
     * @return mixed
     */
    public function actionView($slug)
    {
        $model = $this->findModel($slug);
        return $this->render(
            'view',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Finds the ArticleCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return ArticleCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = ArticleCategory::find()->where('slug=\'' . $slug . '\'')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
