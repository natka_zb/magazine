<?php
/* @var $this yii\web\View */
/* @var $model common\models\ArticleCategory */

?>
<div class="mp-container mp-post-main" itemscope="" itemtype="http://schema.org/Article">
    <h2 class="mp-title" itemprop="headline">
        <a itemprop="mainEntityOfPage" href="/article/<?=$model->slug?>" title="<?=$model->title?>" rel="bookmark">
            <?=$model->title?>
        </a>
    </h2>
    <div class="mp-article-meta">
        <span class="mp-author">
            <a class="author-url" href="/" title="View all posts by Katy Harvill" rel="author">
                <span itemprop="author" itemscope="" itemtype="http://schema.org/Person">
                    <span itemprop="name">By <?=$model->author->username?></span>
                </span>
            </a>
        </span>
        <span class="mp-date">
            <time datetime="<?=Yii::$app->formatter->asDate($model->published_at,"Y-m-dd H:i")?>" itemprop="datePublished"><?=Yii::$app->formatter->asDate($model->published_at,"dd.MM.Y")?></time>
            <time class="meta-date-modified" datetime="<?=Yii::$app->formatter->asDate($model->updated_at,"Y-m-dd H:i")?>" itemprop="dateModified"><?=Yii::$app->formatter->asDate($model->updated_at,"dd.m.Y")?></time>
        </span>
        <span class="mp-views">
            <span class="views-hot" title="Views">
                <i class="fa fa-bolt"></i>
                917
                <meta itemprop="interactionCount" content="UserPageVisits:917">
            </span>
        </span>
    </div>
    <a class="mp-image" href="/article/<?=$model->slug?>" title="<?=$model->title?>" rel="bookmark">
        <div itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
            <img src="<?=$model->thumbnail_base_url . '/' . $model->thumbnail_path?>" srcset="<?=$model->thumbnail_base_url . '/' . $model->thumbnail_path?> 1200w, <?=$model->thumbnail_base_url . '/' . $model->thumbnail_path?> 300w" sizes="(max-width: 600px) 100vw, 600px" alt="" width="600" height="400">
            <meta itemprop="url" content="<?=$model->thumbnail_base_url . '/' . $model->thumbnail_path?>">
            <meta itemprop="width" content="1600">
            <meta itemprop="height" content="1100">
        </div>
    </a>
</div>
