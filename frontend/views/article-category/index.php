<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->title;
echo \yii\widgets\ListView::widget(
    [
        'dataProvider' => $dataProvider,
        'layout' => '<article id="post-813" class="clearfix post-813 page type-page status-publish" style="transform: none;">
                        <div class="entry-content clearfix" style="transform: none;">
                            <div class="vc_row wpb_row vc_row-fluid" style="transform: none;">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="mnky-posts clearfix mp-layout-5 mp-two-column">
                                                {items}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <div class="clear"></div><div id="pager">{pager}</div>',
        'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('view', ['model' => $model]);
            },
        'summary' => true,
        'pager' => [
            'prevPageLabel' => 'Previous',
            'prevPageCssClass' => 'prev_page',
            'nextPageLabel' => 'Next',
            'nextPageCssClass' => 'next_page',
            'activePageCssClass' => 'current',
            'view' => function ($model, $key, $index, $widget) {
                return $this->render('_pager', ['model' => $model]);
            },
        ]
    ]
);
?>
