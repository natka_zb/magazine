<?php
/* @var $this \yii\web\View */
use yii\helpers\Html;
//use frontend\assets\MainAsset;
use app\widgets\MenuBottomWidget;
use app\widgets\MenuTopWidget;

//MainAsset::register($this);
?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Expert advice, sex tips, relationship advice and more."/>
        <meta name="keywords"
              content="sex, sexuality, femininity, womanhood, cuddling, kissing, lovemaking, sexual activity, intimacy, relations, sexual relations, fornication, sexual intercourse, cheating, condoms, love lessons, sexting, rebound sex"/>
        <title><?php echo Html::encode($this->title) ?></title>

        <link rel='stylesheet' href="/css/main.css" />

        <link rel='stylesheet' id='self-maincss-css'
              href='http://www.self.com/wp-content/themes/self-1.0.0/styles/css/screen.min.css?ver=1470687876'
              type='text/css' media='all'/>
        <link rel='stylesheet' href="/css/hush.css" />
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="http://mnkythemes.com/hush/xmlrpc.php">
        <link rel='dns-prefetch' href='//fonts.googleapis.com' />
        <link rel="alternate" type="application/rss+xml" title="Hush &raquo; Feed" href="http://mnkythemes.com/hush/feed/" />
        <link rel="alternate" type="application/rss+xml" title="Hush &raquo; Comments Feed" href="http://mnkythemes.com/hush/comments/feed/" />
        <link rel='stylesheet' id='ot-google-fonts-css'  href='//fonts.googleapis.com/css?family=Roboto:300,300italic,regular,italic,500,500italic,700,700italic' type='text/css' media='all' />
        <link rel='https://api.w.org/' href='http://mnkythemes.com/hush/wp-json/' />
        <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://mnkythemes.com/hush/xmlrpc.php?rsd" />
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://mnkythemes.com/hush/wp-includes/wlwmanifest.xml" />
        <link rel="canonical" href="http://mnkythemes.com/hush/" />
        <link rel='shortlink' href='http://mnkythemes.com/hush/' />
        <link rel="alternate" type="application/json+oembed" href="http://mnkythemes.com/hush/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fmnkythemes.com%2Fhush%2F" />
        <link rel="alternate" type="text/xml+oembed" href="http://mnkythemes.com/hush/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fmnkythemes.com%2Fhush%2F&#038;format=xml" />
        <link rel="icon" href="http://mnkythemes.com/hush/wp-content/uploads/2016/02/cropped-favicon-32x32.png" sizes="32x32" />
        <link rel="icon" href="http://mnkythemes.com/hush/wp-content/uploads/2016/02/cropped-favicon-192x192.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="http://mnkythemes.com/hush/wp-content/uploads/2016/02/cropped-favicon-180x180.png" />
        <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="http://mnkythemes.com/hush/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen">
        <![endif]-->
        <!--[if IE  8]>
        <link rel="stylesheet" type="text/css" href="http://mnkythemes.com/hush/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen">
        <![endif]-->

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!--<script type="text/javascript" src="/frontend/web/js/main.js"></script>-->
        <!--<script type="text/javascript" src="http://mnkythemes.com/hush/wp-content/cache/minify/000000/nctLCoRADEXRDWmFXpJWhU4k9elKorh7FRw4ExoevMk95EqwtZFLFE-osJz7Ofb9voHeijHzt0-GIXMZPtBxVeGEHZrPwhEmVbQHC0aYsXmJFKxW0X_gGVdx41oufQA.js"></script>-->
        <!--<script type="text/javascript" src="http://mnkythemes.com/hush/wp-content/cache/minify/000000/TcxBDoAwCETRC1lJT2RIi4kmMAaIXt8uWc9_0-kDBlTFhxBHSAbdQafDUmwSz9kSbbDnrpdtfa3HAg9CvIB3xfCod63YHw.js"></script>-->
        <!--<script type="text/javascript" src="http://mnkythemes.com/hush/wp-content/cache/minify/000000/M9RPzs8rSUwu0U3LL8rVNdfPzEvOKU1JLdbPAqLC0tSiSj2QjF5uZh4A.js"></script>-->
        <!--<script type="text/javascript" src="http://mnkythemes.com/hush/wp-content/cache/minify/000000/LYzBDcIwDAAXwrX6YgYGYIBijOo2jovtEGV7UMXzdKebkazmQgkvc4UrSqXSnhy4BQa5HBmXGbXuA3JlZTicP8L99DkKQ3RJWtl_WTcjU2UnxiWC89xs78Y-4FGM9iZ_nE683yaV-gU.js"></script>-->
        <!--<script type="text/javascript" src="http://mnkythemes.com/hush/wp-content/cache/minify/000000/M9Qvz89Pzs_NTS1KTtVPLC5OLSnWzyrWTyvKzytJzUtBltbLzczTMcShIauwNLWoUjc5Pz87MxXK04PwQPoA.js"></script>-->
        <!--<script type="text/javascript" src="http://mnkythemes.com/hush/wp-content/cache/minify/000000/M9RPTSyu1E1LzEuuTMqv0IczsgpLU4sq9WB8XUM9Yz1zvdzMPAA.js"></script>-->
        <!--<script type="text/javascript" src="http://mnkythemes.com/hush/wp-content/cache/minify/000000/M9RPTSyu1E1LzEuuTMqv0M8qLE0tqtTLzS8tTi3PSE3N0cvNzAMA.js"></script>-->

        <script type="text/javascript" src="/js/subMenu.js"></script>

        <style type="text/css">
            .essb_links_list li.essb_totalcount_item .essb_t_l_big .essb_t_nb:after, .essb_links_list li.essb_totalcount_item .essb_t_r_big .essb_t_nb:after { color: #777777;content: "Shares";display: block;font-size: 11px;font-weight: normal;text-align: center;text-transform: uppercase;margin-top: -5px; } .essb_links_list li.essb_totalcount_item .essb_t_l_big, .essb_links_list li.essb_totalcount_item .essb_t_r_big { text-align: center; }.essb_displayed_sidebar .essb_links_list li.essb_totalcount_item .essb_t_l_big .essb_t_nb:after, .essb_displayed_sidebar .essb_links_list li.essb_totalcount_item .essb_t_r_big .essb_t_nb:after { margin-top: 0px; } .essb_displayed_sidebar_right .essb_links_list li.essb_totalcount_item .essb_t_l_big .essb_t_nb:after, .essb_displayed_sidebar_right .essb_links_list li.essb_totalcount_item .essb_t_r_big .essb_t_nb:after { margin-top: 0px; } .essb_totalcount_item_before, .essb_totalcount_item_after { display: block !important; }.essb_totalcount_item_before .essb_totalcount, .essb_totalcount_item_after .essb_totalcount { border: 0px !important; }.essb_counter_insidebeforename { margin-right: 5px; font-weight: bold; }.essb_width_columns_1 li { width: 100%; }.essb_width_columns_1 li a { width: 92%; }.essb_width_columns_2 li { width: 49%; }.essb_width_columns_2 li a { width: 86%; }.essb_width_columns_3 li { width: 32%; }.essb_width_columns_3 li a { width: 80%; }.essb_width_columns_4 li { width: 24%; }.essb_width_columns_4 li a { width: 70%; }.essb_width_columns_5 li { width: 19.5%; }.essb_width_columns_5 li a { width: 60%; }.essb_width_columns_6 li { width: 16%; }.essb_width_columns_6 li a { width: 55%; }.essb_links li.essb_totalcount_item_before, .essb_width_columns_1 li.essb_totalcount_item_after { width: 100%; text-align: left; }.essb_network_align_center a { text-align: center; }.essb_network_align_right .essb_network_name { float: right;}
        </style>
        <style type="text/css" data-type="vc_shortcodes-custom-css">
            .vc_custom_1459854127213{padding-top: 40px !important;padding-bottom: 40px !important;background-color: #f7f7fc !important;}.vc_custom_1459859139676{padding-left: 40px !important;}.vc_custom_1459865149175{padding-top: 20px !important;padding-right: 20px !important;padding-left: 20px !important;background-color: #f7f7fc !important;}.vc_custom_1459864975973{padding-top: 20px !important;padding-left: 40px !important;}
        </style>
        <style type="text/css">
            .wpb_animate_when_almost_visible { opacity: 1; }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="top-bar-wrapper" class="clearfix">
                <div id="top-bar" itemscope itemtype="http://schema.org/WPSideBar">
                    <!--<div id="topleft-widget-area">
                        <ul>
                            <li id="text-4" class="widget-container widget_text">
                                <div class="textwidget">
                                    <div style="margin-top:-5px;">
                                        <span class="mnky-font-icon change-color" style="color:#db0a5b;">
                                            <i class="typcn typcn-social-facebook" style="font-size:22px; color:#333333; padding-left:0px; padding-right:10px;"></i>
                                        </span>
                                        <span class="mnky-font-icon change-color" style="color:#db0a5b;">
                                            <i class="typcn typcn-social-twitter" style="font-size:22px; color:#333333; padding-left:0px; padding-right:10px;"></i>
                                        </span>
                                        <span class="mnky-font-icon change-color" style="color:#db0a5b;">
                                            <i class="typcn typcn-social-google-plus" style="font-size:22px; color:#333333; padding-left:0px; padding-right:10px;"></i>
                                        </span>
                                        <span class="mnky-font-icon change-color" style="color:#db0a5b;">
                                            <i class="typcn typcn-social-pinterest" style="font-size:22px; color:#333333; padding-left:0px; padding-right:0px;"></i>
                                        </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>-->
                    <div id="topright-widget-area" class="clearfix">
                        <ul>
                            <li id="nav_menu-2" class="widget-container widget_nav_menu">
                                <div class="menu-top-bar-menu-container">
                                    <ul id="menu-top-bar-menu" class="menu">
                                        <li id="menu-item-222" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-222">
                                            <a href="/">About Us</a>
                                        </li>
                                        <li id="menu-item-744" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-744">
                                            <a href="/">Advertise</a>
                                        </li>
                                        <li id="menu-item-220" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-220">
                                            <a href="/">Contact Us</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <header id="mobile-site-header" class="mobile-header">
                <div id="mobile-site-logo">
                    <a href="/">
                        <img src="http://mnkythemes.com/hush/wp-content/uploads/2016/01/logo_mobile_hush.png" width="122" height="70" alt="Hush" class="default-logo" />
                        <img src="http://mnkythemes.com/hush/wp-content/uploads/2016/01/logo_mobile_hush@2x.png" width="122" height="70" alt="Hush" class="retina-logo" />
                    </a>
                </div>
                <a href="#mobile-site-navigation" class="toggle-mobile-menu">
                    <i class="fa fa-bars"></i>
                </a>
            </header>
            <header id="site-header" class="header-style-1" itemscope itemtype="http://schema.org/WPHeader">
                <div id="header-wrapper">
                    <div id="header-container" class="clearfix">
                        <div id="site-logo">
                            <a href="/">
                                <img src="http://mnkythemes.com/hush/wp-content/uploads/2016/01/logo_hush.png" width="173" height="90" alt="Hush" class="default-logo" />
                                <img src="http://mnkythemes.com/hush/wp-content/uploads/2016/01/logo_hush@2x.png" width="173" height="90" alt="Hush" class="retina-logo" />
                            </a>
                        </div>
                        <div id="header-sidebar" class="clearfix">
                            <ul class="header-widget-area">
                                <li id="text-6" class="widget-container widget_text">
                                    <div class="textwidget">
                                        <aside itemscope itemtype="https://schema.org/WPAdBlock" class="site-commerc hide-ad" >
                                            <div class="commercial responsive-hide" style="max-width:728px;max-height:none;">
                                                <a href="#" target="_self" rel=nofollow itemprop="url">
                                                    <img class="commerc-img" width=728 height=90 src="http://mnkythemes.com/hush/wp-content/uploads/2016/02/ad_728-90-3.png" alt="Demo image" itemprop="image">
                                                </a>
                                            </div>
                                            <div class="commercial responsive-show" style="max-width:468px;">
                                                <a href="#" target="_self" rel=nofollow itemprop="url"> <img width=468 height=60 src="http://mnkythemes.com/hush/wp-content/uploads/2016/02/ad_468-60_with_paddings.png" alt="Demo image" itemprop="image"></a>
                                            </div>
                                        </aside>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div><!-- #header-container -->
                </div><!-- #header-wrapper -->
            </header><!-- #site-header -->
            <div id="navigation-wrapper" class="header-style-1">
                <div id="navigation-container">
                    <div id="navigation-inner" class="clearfix">
                        <nav id="site-navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                            <div class="menu-container">
                                <?=MenuTopWidget::widget()?>
                            </div>
                        </nav><!-- #site-navigation -->
                        <div id="site-utility">
                            <div class="header_cart_wrapper">
                                <a href="http://mnkythemes.com/hush/cart/" title="View your shopping cart" class="header_cart_link" >
                                    <span class="header_cart_button">
                                        <i class="fa fa-shopping-cart"></i>
                                    </span>
                                </a>
                                <div class="header_cart_widget">
                                    <div class="widget woocommerce widget_shopping_cart">
                                        <div class="widget_shopping_cart_content"></div>
                                    </div>
                                </div>
                            </div>
                            <!--<button id="trigger-header-search" class="search_button" type="button">
                                <i class="fa fa-search"></i>
                            </button> -->
                        </div>
                        <!--<div class="header-search">
                            <div class="searchform-wrapper">
                                <form method="get" class="searchform" action="http://mnkythemes.com/hush/">
                                    <input onfocus="this.value=''" onblur="this.value='Type and hit enter to search ...'" type="text" value="Type and hit enter to search ..." name="s" class="search-input" />
                                </form>
                            </div>
                        </div>-->
                    </div><!-- #navigation-inner -->
                </div><!-- #navigation-container -->
            </div><!-- #navigation-wrapper -->
            <div id="main" class="clearfix" style="transform: none;">
                <div id="container" style="transform: none;">
                    <?=$content?>
                </div>
            </div>
            <footer class="site-footer" id="site-footer" itemscope itemtype="http://schema.org/WPFooter">
                <div class="footer-sidebar clearfix" itemscope itemtype="http://schema.org/WPSideBar">
                    <div class="inner">
                        <div class="vc_row wpb_row">
                            <div class="vc_col-sm-3">
                                <div class="widget-area">
                                    <div id="text-13" class="widget widget_text">
                                        <div class="textwidget">
                                            <img src="http://mnkythemes.com/hush/wp-content/uploads/2016/02/logo_white@2x.png" width="155" height="51" alt="Footer logo" />
                                        </div>
                                    </div>
                                    <div id="text-3" class="widget widget_text">
                                        <div class="textwidget">
                                            <div style="padding-right:100px; padding-bottom:20px;">Entertainment and celebrity gossip magazine theme. Start writing, publishing, advertising and sharing in minutes with pre-defined options and layouts or start customizing and be surprised by our theme flexibility. More news, less hassle…</div>
                                            <!--<span style="margin-right:15px;">Follow us:</span>
                                            <span class="mnky-font-icon change-color" style="color:#ffffff;">
                                                <i class="typcn typcn-social-facebook" style="font-size:30px; color:#333333; padding-left:0px; padding-right:10px;"></i>
                                            </span>
                                            <span class="mnky-font-icon change-color" style="color:#ffffff;">
                                                <i class="typcn typcn-social-twitter" style="font-size:30px; color:#333333; padding-left:0px; padding-right:10px;"></i>
                                            </span>
                                            <span class="mnky-font-icon change-color" style="color:#ffffff;">
                                                <i class="typcn typcn-social-google-plus" style="font-size:30px; color:#333333; padding-left:0px; padding-right:10px;"></i>
                                            </span>
                                            <span class="mnky-font-icon change-color" style="color:#ffffff;">
                                                <i class="typcn typcn-social-pinterest" style="font-size:30px; color:#333333; padding-left:0px; padding-right:0px;"></i>
                                            </span>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_col-sm-3">
                                <div class="widget-area">
                                    <div id="categories-3" class="widget widget_categories">
                                        <?=MenuBottomWidget::widget()?>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .vc_row -->
                    </div><!-- .inner -->
                </div><!-- .footer-sidebar -->
                <div class="site-info" itemscope itemtype="http://schema.org/WPSideBar">
                    <div class="inner">
                        <div id="text-2" class="widget widget_text">
                            <div class="textwidget">© <?=date("Y")?> Hush Media Group, Inc. All rights reserved.</div>
                        </div>
                    </div>
                </div>
            </footer><!-- .site-footer -->
            <div id="mobile-menu-bg"></div>
        </div><!-- #wrapper -->
        <nav id="mobile-site-navigation">
            <span class="mobile-menu-header">
                <span class="mobile-menu-heading">Menu</span>
                <i class="fa fa-times toggle-mobile-menu"></i>
            </span>
            <ul id="menu-mobile-menu" class="menu">
                <li id="menu-item-922" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-922">
                    <a href="#">Homepages</a>
                    <span></span>
                    <ul class="sub-menu">
                        <li id="menu-item-931" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-813 current_page_item menu-item-931"><a href="http://mnkythemes.com/hush/">Main Homepage</a><span></span></li>
                        <li id="menu-item-930" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-930"><a href="http://mnkythemes.com/hush/article-feed/">Article Feed</a><span></span></li>
                        <li id="menu-item-927" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-927"><a href="http://mnkythemes.com/hush/static-slider/">Static Slider</a><span></span></li>
                        <li id="menu-item-929" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-929"><a href="http://mnkythemes.com/hush/news-slider/">News Slider</a><span></span></li>
                        <li id="menu-item-928" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-928"><a href="http://mnkythemes.com/hush/minimalistic/">Minimalistic</a><span></span></li>
                        <li id="menu-item-926" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-926"><a href="http://mnkythemes.com/hush/only-grid/">Only Grid</a><span></span></li>
                        <li id="menu-item-925" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-925"><a href="http://mnkythemes.com/hush/article-cards/">Article Cards</a><span></span></li>
                        <li id="menu-item-923" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-923"><a href="http://mnkythemes.com/hush/intense/">Intense</a><span></span></li>
                    </ul>
                </li>
                <li id="menu-item-803" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-803"><a href="http://mnkythemes.com/hush/category/celebrity-news/">Celebrity News</a><span></span></li>
                <li id="menu-item-804" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-804"><a href="http://mnkythemes.com/hush/category/lifestyle/">Lifestyle</a><span></span></li>
                <li id="menu-item-805" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-805"><a href="http://mnkythemes.com/hush/category/style/">Style</a><span></span></li>
                <li id="menu-item-806" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-806"><a href="http://mnkythemes.com/hush/category/pics/">Pics</a><span></span></li>
                <li id="menu-item-807" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-807"><a href="http://mnkythemes.com/hush/category/videos/">Videos</a><span></span></li>
                <li id="menu-item-932" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-932"><a href="http://mnkythemes.com/hush/about-us/">About Us</a><span></span></li>
                <li id="menu-item-933" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-933"><a href="http://mnkythemes.com/hush/contact-us/">Contact Us</a><span></span></li>
            </ul>
            <div id="mobile-menu-sidebar" class="clearfix">
                <div id="search-2" class="widget widget_search">
                    <div class="searchform-wrapper">
                        <form method="get" class="searchform" action="http://mnkythemes.com/hush/">
                            <input onfocus="this.value=''" onblur="this.value='Type and hit enter to search ...'" type="text" value="Type and hit enter to search ..." name="s" class="search-input" />
                        </form>
                    </div>
                </div>
            </div>
        </nav><!-- #mobile-site-navigation -->
        <a href="#top" class="scrollToTop">
            <i class="fa fa-angle-up"></i>
        </a>
        <div id="mtp-wrapper">
            <div id="mtp-header">
                <span>Live Demo Assistant</span>
                <i class="mtp-close fa fa-times"></i>
            </div>
            <div id="mtp-content">
                <ul>
                    <li>View our home page examples under Homepages menu.</li>
                    <li>Click on category names in menu to visit category pages.</li>
                    <li>Hover over different menu items for submenu styles.</li>
                    <li>Explore theme features by visiting pages under Features menu.</li>
                    <li>Browse pages, try available actions, have fun!</li>
                </ul>
            </div>
        </div>
        <!--<div class="mtp-open">
            <i class="fa fa-hand-peace-o"></i>
        </div>-->
    </body>
</html>
