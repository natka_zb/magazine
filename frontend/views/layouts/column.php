<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07.09.16
 * Time: 16:55
 */
?>
<div id="m-ad" class="visible-phone">
    <div id="AMS_SLF_MOBILE_GLOBAL_CONTENTRIVER" class="ecom-placement"><a
            href="http://www.self.com/go/mobilefailsafe" target="_blank"><img
                src="http://www.self.com/wp-content/themes/self-1.0.0/images/ecom/SLF_MOBRiver_300x75.jpg"
                alt="Subscribe to Self" border="0"/></a>
        <script type="text/javascript">
            //<!--
            if (( typeof pageAds != 'undefined') && ( CN.url.params('nojoy') != 1 )) {
                (function () {
                    if (typeof pageAds.AMS_SLF_MOBILE_GLOBAL_CONTENTRIVER != 'undefined') {
                        jQuery("#AMS_SLF_MOBILE_GLOBAL_CONTENTRIVER").html(pageAds.AMS_SLF_MOBILE_GLOBAL_CONTENTRIVER.replace(/document.write\(.*\)/gi, "/* filtered by amg-magnet:document.write(...) */"));
                        jQuery("#AMS_SLF_MOBILE_GLOBAL_CONTENTRIVER").css({ visibility: 'visible' });
                    } else {
                        CN.debug.info("AMS_SLF_MOBILE_GLOBAL_CONTENTRIVER not in pageAds.");
                    }
                })();
            }
            //-->
        </script>
    </div>
</div>
<div id="sidebar1" class="fluid-sidebar sidebar pull-right" role="complementary">
    <div>
        <div class="row-fluid">
            <!-- Package page extra content in sidebar start -->
            <!-- Package page extra content in sidebar end -->
            <div id="box-ad-sidebar ads-container-item" class="visible-desktop visible-tablet">
                <span class="advt-slug">Advertisement</span>

                <div class="box-ad-inner">
                    <div class="displayAd300x250Js">
                        <div class="advertisement">
                            <div id="homepage_rightrail_top300x250_frame" class="displayAd displayAd300x250Js"
                                 data-cb-ad-id="homepage_rightrail_top300x250_frame"></div>
                            <script type="text/javascript">if (typeof CN !== "undefined") {
                                    if (!CN.isMobile && CN.dart) {
                                        CN.dart.call("homepage_rightrail_top", { sz: "300x250", kws: [ "top" ], collapse: true });
                                    }
                                }</script>
                        </div>
                    </div>
                </div>

            </div>
            <div class="text">

            </div>

        </div>
    </div>
    <!--flash section end -->
    <div class="row-fluid">
        <div class="visible-phone pagination-centered">
            <div class="displayAd300x51Js">
                <div class="advertisement">
                    <div id="homepage_rightrail_top320x51_frame" class="displayAd displayAd320x51Js"
                         data-cb-ad-id="homepage_rightrail_top320x51_frame"></div>
                    <script type="text/javascript">if (typeof CN !== "undefined") {
                            if (CN.isMobile && CN.dart) {
                                CN.dart.call("homepage_rightrail_top", { sz: ( "320x51" ), kws: [ "bottom" ], collapse: true });
                            }
                        }</script>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hidden-phone">
    <div id="sidebar2" class="fluid-sidebar sidebar span4" role="complementary">
        <!--CM NEWSLETTER BOX -->
        <div class="cm-newsletter-box-right">
            <div id="AMS_SLF_GLOBAL_RR_NEWS" class="ecom-placement"><!-- no failsafe for AMS_SLF_GLOBAL_RR_NEWS -->
                <script type="text/javascript">
                    //<!--
                    if (( typeof pageAds != 'undefined') && ( CN.url.params('nojoy') != 1 )) {
                        (function () {
                            if (typeof pageAds.AMS_SLF_GLOBAL_RR_NEWS != 'undefined') {
                                jQuery("#AMS_SLF_GLOBAL_RR_NEWS").html(pageAds.AMS_SLF_GLOBAL_RR_NEWS.replace(/document.write\(.*\)/gi, "/* filtered by amg-magnet:document.write(...) */"));
                                jQuery("#AMS_SLF_GLOBAL_RR_NEWS").css({ visibility: 'visible' });
                            } else {
                                CN.debug.info("AMS_SLF_GLOBAL_RR_NEWS not in pageAds.");
                            }
                        })();
                    }
                    //-->
                </script>
            </div>
        </div>
        <!-- END CM NEWSLETTER BOX -->
        <div class="rightrail-nav">
            <div class="row-fluid visible-desktop visible-tablet">
                <div id="AMS_SLF_GLOBAL_RIGHTRAILNAV" class="ecom-placement">
                    <div class="self-cm">Self</div>
                    <ul class="yrail-subs-nav">
                        <li id="snav1"><a href="http://www.self.com/go/failsafe" target="_blank"
                                          title="Give a Subscription to Self magazine as a Gift">Subscribe</a></li>
                        <li id="snav2"><a href="http://www.self.com/go/giftfailsafe" target="_blank"
                                          title="Give a Subscription to Self magazine as a Gift">Gift</a></li>
                        <li id="snav3"><a
                                href="https://w1.buysub.com/pubs/N3/SLF/Register.jsp?cds_page_id=175534&cds_mag_code=SLF"
                                target="_blank" title="Renew your Subscription to Self magazine">Renew</a></li>
                        <li id="snav4"><a
                                href="https://w1.buysub.com/pubs/N3/SLF/Register.jsp?cds_page_id=175534&cds_mag_code=SLF"
                                target="_blank" title="Subscription Questions and Answers for Self magazine">Questions</a>
                        </li>
                    </ul>
                    <script type="text/javascript">
                        //<!--
                        if (( typeof pageAds != 'undefined') && ( CN.url.params('nojoy') != 1 )) {
                            (function () {
                                if (typeof pageAds.AMS_SLF_GLOBAL_RIGHTRAILNAV != 'undefined') {
                                    jQuery("#AMS_SLF_GLOBAL_RIGHTRAILNAV").html(pageAds.AMS_SLF_GLOBAL_RIGHTRAILNAV.replace(/document.write\(.*\)/gi, "/* filtered by amg-magnet:document.write(...) */"));
                                    jQuery("#AMS_SLF_GLOBAL_RIGHTRAILNAV").css({ visibility: 'visible' });
                                } else {
                                    CN.debug.info("AMS_SLF_GLOBAL_RIGHTRAILNAV not in pageAds.");
                                }
                            })();
                        }
                        //-->
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>