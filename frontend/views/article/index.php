<?php
/* @var $this yii\web\View */
$this->title = Yii::t('frontend', 'Articles')
?>

<div class="span12 toprail hidden-phone">
    <!--top rail section -->
    <div class="most-views"><ol><li class="most_pop_odd "><a href="http://www.self.com/fitness/2016/08/a-4-minute-abs-series-you-can-do-before-breakfast/"><div class="most-image-warpper"><img src="https://images.parsely.com/CbPp9tiSzATE7wgW5GMJ71Rx0Rc=/85x85/smart/http%3A//www.self.com/wp-content/uploads/2016/08/ab-series-do-before-breakfast_feature-225x225.png"></div></a><a class="link_most_views" href="http://www.self.com/fitness/2016/08/a-4-minute-abs-series-you-can-do-before-breakfast/">A 4-Minute Abs Series You Can Do Before Breakfast</a></li><li class="most_pop_even most-popular-native-content"><a href="http://www.self.com/trending/2016/09/laurie-hernandez-debut-dancing-with-the-stars/"><div class="most-image-warpper"><img src="https://images.parsely.com/SrgtszoQ5UwW78lhad9IbiAyPtM=/85x85/smart/http%3A//www.self.com/wp-content/uploads/2016/09/GettyImages-603569084-225x225.jpg"></div></a><a class="link_most_views" href="http://www.self.com/trending/2016/09/laurie-hernandez-debut-dancing-with-the-stars/">Watch Laurie Hernandez Debut On ‘Dancing With The Stars,’ Prepare To Be Amazed</a></li><li class="most_pop_odd "><a href="http://www.self.com/trending/2016/09/the-fifty-shades-darker-trailer-is-officially-here/"><div class="most-image-warpper"><img src="https://images.parsely.com/zAHs2M4K5ZJb9bba-HiM6rwmCfw=/85x85/smart/http%3A//www.self.com/wp-content/uploads/2016/09/Fifty-Shades_Feat-225x225-1473789401.jpg"></div></a><a class="link_most_views" href="http://www.self.com/trending/2016/09/the-fifty-shades-darker-trailer-is-officially-here/">The ‘Fifty Shades Darker’ Trailer Is Officially Here</a></li><li class="most_pop_even "><a href="http://www.self.com/body/fitness/2014/01/essential-stretches-slideshow/"><div class="most-image-warpper"><img src="https://images.parsely.com/vs4de9nIbEorwOdMO9O0ciLWqyk=/85x85/smart/http%3A//www.self.com/wp-content/uploads/2015/09/Essential-Stretches-800-225x225.jpg"></div></a><a class="link_most_views" href="http://www.self.com/body/fitness/2014/01/essential-stretches-slideshow/">The 10 Best Stretches For Better Flexibility</a></li><li class="most_pop_odd "><a href="http://www.self.com/trending/2016/09/greys-anatomy-actress-sara-ramirez-got-a-seriously-dramatic-buzzcut/"><div class="most-image-warpper"><img src="https://images.parsely.com/dZ7KHlsfshUhfSx5QEjgcPELc9M=/85x85/smart/http%3A//www.self.com/wp-content/uploads/2016/09/Sara-Ramirez-Buzzcut-225x225-1473778702.jpg"></div></a><a class="link_most_views" href="http://www.self.com/trending/2016/09/greys-anatomy-actress-sara-ramirez-got-a-seriously-dramatic-buzzcut/">‘Grey’s Anatomy’ Actress Sara Ramirez Got A Seriously Dramatic Buzzcut</a></li></ol></div>        <div class="global_primary_unit" style="display: none;">
        <li class="most-popular-native-content-0 most_pop_even">
            <a class="js-track-url" href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/"
               data-tablet-href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/"
               data-mobile-href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/" >

                <div class="most-image-warpper"><img src="http://www.self.com/wp-content/uploads/2016/08/IMG_9109-220x220.jpg" alt="IMG_9109" data-lazy-loaded="false" /></div>
            </a>
            <div class="link_most_views">
                <div class="slf-sponsored-name">
                    <span class="sp-title">Sponsored Content:</span>
                    <span class="sp-name">H&M Sport</span>
                </div>
                <a class="link_most_views js-track-url" href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/"
                   data-tablet-href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/"
                   data-mobile-href="http://www.self.com/native-article/woman-teaches-cycling-classes-finds-her-motivation/">Where a Woman Who Teaches 15 Cycling Classes a Week Finds Her Motivation—And Where You Can Find Yours</a>
            </div>
        </li>

        <div style="display: none;">
            <input type="text" class="datapos60x60" value="0"/>
            <div id="global_primary_unit60x60_frame"></div>
            <script type="text/javascript">
                CN.dart.call( "global_primary_unit", { sz: "60x60", kws: [ "" ] } );
            </script>
        </div>
        <li class="most-popular-native-content-1 most_pop_even">
            <a class="js-track-url" href="http://www.self.com/native-article/my-best-accessory/"
               data-tablet-href="http://www.self.com/native-article/my-best-accessory/"
               data-mobile-href="http://www.self.com/native-article/my-best-accessory/" >

                <div class="most-image-warpper"><img src="http://www.self.com/wp-content/uploads/2016/06/20160607-25-220x220-1466606482.jpg" alt="20160607-25" data-lazy-loaded="false" /></div>
            </a>
            <div class="link_most_views">
                <div class="slf-sponsored-name">
                    <span class="sp-title">Sponsored Content</span>
                    <span class="sp-name">G-Shock</span>
                </div>
                <a class="link_most_views js-track-url" href="http://www.self.com/native-article/my-best-accessory/"
                   data-tablet-href="http://www.self.com/native-article/my-best-accessory/"
                   data-mobile-href="http://www.self.com/native-article/my-best-accessory/">My Best Accessory </a>
            </div>
        </li>

        <div style="display: none;">
            <input type="text" class="datapos90x92" value="1"/>
            <div id="global_primary_unit90x92_frame"></div>
            <script type="text/javascript">
                CN.dart.call( "global_primary_unit", { sz: "90x92", kws: [ "" ] } );
            </script>
        </div>
    </div>
</div>


<div class="row-fluid">
    <div class="span8 center_well_tab chrono-feed">
        <div class="center_well">
        </div>
    </div>
    <div class="span4 centre_well_sb_1">
        <?php $this->beginContent('@app/views/layouts/column.php'); ?>
        <?php $this->endContent(); ?>
    </div>
</div>

<div id="article-index">
    <h1><?php echo Yii::t('frontend', 'Articles') ?></h1>
    <?php echo \yii\widgets\ListView::widget([
        'dataProvider'=>$dataProvider,
        'pager'=>[
            'hideOnSinglePage'=>true,
        ],
        'itemView'=>'_item'
    ])?>
</div>