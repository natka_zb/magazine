<?php
/* @var $this yii\web\View */
/* @var $model common\models\Article */
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<article itemtype="http://schema.org/Article" itemscope="" id="oneArticle" class="single-layout clearfix post-533 post type-post status-publish format-video has-post-thumbnail hentry category-lifestyle category-videos tag-celebs tag-exclusive" >
    <link itemprop="mainEntityOfPage" href="/article/<?=$model->slug?>" />
    <header class="entry-header clearfix">
        <h5>
            <a href="/<?=$model->category->slug?>" rel="category tag"><?=$model->category->title?></a>
        </h5>
        <h1 itemprop="headline" class="entry-title"><?=$model->title?></h1>
        <div class="entry-meta">
            <span class="meta-author-image">
                <img alt='' src='http://0.gravatar.com/avatar/f13c50fc0820706943f19d3f5454d086?s=50&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/f13c50fc0820706943f19d3f5454d086?s=100&amp;d=mm&amp;r=g 2x' class='avatar avatar-50 photo' height='50' width='50' />
            </span>
            <a class="meta-author url" href="/" title="View all posts by <?=$model->author->username?>" rel="author">
                <span itemprop="author" itemscope itemtype="http://schema.org/Person">
                    <span itemprop="name"><?=$model->author->username?></span>
                </span>
            </a>
            <span class="meta-date">
                <time class="published" datetime="<?=Yii::$app->formatter->asDate($model->published_at,"Y-m-dd H:i")?>" itemprop="datePublished"><?=Yii::$app->formatter->asDate($model->published_at,"dd.MM.Y")?></time>
                <time class="meta-date-modified updated" datetime="<?=Yii::$app->formatter->asDate($model->updated_at,"Y-m-dd H:i")?>" itemprop="dateModified"><?=Yii::$app->formatter->asDate($model->updated_at,"dd.MM.Y")?></time>
            </span>
            <meta itemprop="interactionCount" content="UserComments:0"/>
            <span class="meta-views">
                <span class="views-hot" title="Views">
                    <i class="fa fa-bolt"></i>
                    944
                    <meta itemprop="interactionCount" content="UserPageVisits:944"/>
                </span>
            </span>
        </div>
    </header><!-- .entry-header -->
    <div itemprop="articleBody" class="entry-content">
        <?=$model->body?>
    </div><!-- .entry-content -->
</article><!-- #oneArticle -->
